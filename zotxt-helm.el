;;; zotxt-helm.el --- Helm selection of zotxt citations  -*- lexical-binding: t -*-

;; Copyright (C) 2017 Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Version: 0.1
;; Created: 2017-07-21
;; Modified: 2021-04-26
;; Package-Requires: ((emacs "25.1") (org "9.0") (zotxt "6.0") (helm "0.0"))
;; Keywords: outlines, wp
;; URL: https://www.gitlab.com/andersjohansson/zotxt-emacs-extra

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; zotxt-helm provides an interface for inserting citations using helm
;; completion framework

;;; Code:
(require 'org-zotxt)
(require 'zotxt-extra)
(require 'helm)
(require 'helm-mode)
(require 'helm-utils)
(eval-when-compile
  (require 'cl-lib))

;;;; Variables
(defcustom zotxt-helm-input-idle-delay 0.2
  "Value for `helm-input-idle-delay' when calling
  `zotxt-helm-insert-reference-link'. A small delay prevents
  server calls at every keypress."
  :group 'zotxt-extra
  :type 'float)

(defcustom zotxt-helm-method 'sync
  "Method for retrieving zotxt results, which kind of helm source will be used."
  :group 'zotxt-extra
  :type '(choice (const  :tag "Async source" async)
                 (const :tag "Sync source" sync)))

(defcustom zotxt-helm-current-refs-in-widened-buffer t
  "If non-nil, gathers current references from the widened buffer."
  :type 'boolean
  :group 'zotxt-extra)



(define-key org-zotxt-mode-map (kbd "C-c \" h") #'zotxt-helm-insert-reference-link)

;;;; Functions

(defun zotxt-helm--sync-search ()
  (let (retval
        (process-connection-type nil)
        (request-backend 'url-retrieve))
    (request
      (format "%s/search" zotxt-url-base)
      :params `(("q" . ,helm-pattern)
                ("method" . "titleCreatorYear")
                ("format" . "quickBib"))
      :timeout 2
      :parser #'zotxt-helm--json-parse
      :sync t
      :error (cl-function (lambda (&key _data response &allow-other-keys)
                            (message "zotxt-helm-search failed! %s" response)))
      :success (cl-function
                (lambda (&key data &allow-other-keys)
                  (setq retval (cl-loop for res in data collect
                                        (let-alist res
                                          (cons .quickBib .key)))))))
    retval))

(defun zotxt-helm--json-parse ()
  (if (functionp 'json-parse-buffer)
      (json-parse-buffer :object-type 'alist :array-type 'list)
    (zotxt--json-read)))

(defun zotxt-helm--async-search-process ()
  (let ((process-connection-type nil))
    (prog1 (start-process-shell-command
            "zotxt"
            helm-buffer
            (format "curl --silent '%s/search?q=%s&method=titleCreatorYear&format=quickBib'| jq -r --unbuffered '.[] | \"\\(.quickBib)\\t \\(.key)\"'"
                    zotxt-url-base (url-hexify-string helm-pattern)))
      (set-process-sentinel
       (get-buffer-process helm-buffer)
       (lambda (_process event)
         (let* (;; (err      (process-exit-status process))
                ;; (noresult (or (= err 1)))
                )
           (when (string= event "finished\n")
             (helm-maybe-show-help-echo)
             (with-helm-window
               (setq mode-line-format
                     `(" " mode-line-buffer-identification " "
                       (:eval (format "L%s" (helm-candidate-number-at-point))) " "
                       (:eval (propertize
                               (format
                                "[helm-zotxt process finished (%s results)] "
                                (helm-get-candidate-number))
                               'face 'helm-grep-finish))))
               (force-mode-line-update)
               (when helm-allow-mouse
                 (helm--bind-mouse-for-selection helm-selection-point))))))))))

;;; Source for zotxt

(defvar zotxt-helm-history nil "History for zotxt-helm")
(defvar zotxt-helm-actions
  '(("Insert" . zotxt-helm-retrieve-links)
    ("Insert special citation" . zotxt-helm-retrieve-links-special)
    ("Insert with prefix" . zotxt-helm-retrieve-links-add-prefix)
    ("Insert bibliography of marked" . zotxt-helm-retrieve-fullbib)))

(defun zotxt-helm-source ()
  (if (eq zotxt-helm-method 'sync)
      (helm-build-sync-source "Zotxt"
        :candidates #'zotxt-helm--sync-search
        :requires-pattern 3
        :match-dynamic t
        :history 'zotxt-helm-history
        :candidate-number-limit 9999
        :action zotxt-helm-actions)
    (helm-build-async-source "Zotxt"
      :candidates-process #'zotxt-helm--async-search-process
      :requires-pattern 3
      ;; :display-to-real #'zotxt-helm-after-tab
      :filter-one-by-one #'zotxt-helm-one-by-one
      :history 'zotxt-helm-history
      :candidate-number-limit 9999
      :action zotxt-helm-actions)))

(defun zotxt-helm-one-by-one (line)
  (if-let ((sp (split-string line "	" t " ")))
      (progn
        (cons (car sp) (cadr sp)))
    line))

;;;; Helm source for current buffer
(defun zotxt-helm-get-current-refs ()
  (cl-remove-duplicates
   (org-element-map
       (if zotxt-helm-current-refs-in-widened-buffer
           (save-restriction
             (widen)
             (org-element-parse-buffer))
         (org-element-parse-buffer))
       'link #'zotxt-helm-link-to-disp-real)
   :test 'equal))

(defun zotxt-helm-print-current-refs (sort?)
  (interactive "P")
  (let* ((refs (zotxt-helm-get-current-refs)))
    (when sort?
      (setq refs (cl-sort refs #'string< :key #'car)))
    (insert (mapconcat #'cdr refs "\n"))))

(require 'ox-org)

(defun zotxt-helm-link-to-disp-real (link)
  (when (string= "zotero" (org-element-property :type link))
    (save-match-data
      (when-let ((path (org-element-property :path link))
                 ;; these won’t work when just sending in a string
                 ;; through link-strings-to-disp-real
                 ;; (cbeg (org-element-property :contents-begin link))
                 ;; (cend (org-element-property :contents-end link))
                 ;; (desc (buffer-substring-no-properties cbeg cend))

                 ;; This is perhaps a kind of stupid way to get the
                 ;; description though:
                 (desc (substring-no-properties
                        (org-export-data-with-backend
                         `(paragraph
                           nil
                           ,(cddr link))
                         'org nil)))
                 (lp (zotxt-extra-link-parse-desc desc))
                 (ck (or (plist-get lp :citekey) desc)))
        (cons ck (concat "[[zotero:" path "][@" ck "]]"))))))

(defun zotxt-helm-link-strings-to-disp-real (linkstrings)
  (org-element-map
      (org-element-parse-secondary-string linkstrings '(link))
      'link #'zotxt-helm-link-to-disp-real))

(defvar zotxt-helm-current-refs-source
  (helm-build-sync-source
      "References in current buffer"
    :candidates 'zotxt-helm-current-refs
    :fuzzy-match t
    :action zotxt-helm-actions))

(defvar zotxt-helm-current-refs nil)

;;; History source

(defvar zotxt-helm-history '())
(put 'zotxt-helm-history 'history-length 300)
(when (boundp 'savehist-additional-variables)
  (add-to-list 'savehist-additional-variables
               'zotxt-helm-history))

(defvar zotxt-helm-history-source
  (helm-build-sync-source
      "Previously inserted references"
    :candidates 'zotxt-helm-history
    :fuzzy-match t
    :action zotxt-helm-actions))

(defun zotxt-helm-retrieve-links (c &optional use-c)
  ;; this "use-c" thing is a workaround for ‘zotxt-helm-retrieve-links-special’,
  ;; where ‘helm-marked-candidates’ seem to get confused at times
  ;; (due to  nested helm-sessions I believe)
  (let ((cands (if use-c c (helm-marked-candidates :all-sources t))))
    (zotxt-mapcar-deferred
     (lambda (cand)
       (if (char-equal ?\[ (string-to-char cand))
           (deferred:callback-post (deferred:new) cand)
         (org-zotxt-get-item-link-text-deferred `(:key ,cand)))) ;; only real dependence on deferred
     cands)))

(defvar zotxt-helm-prefix nil)

(defun zotxt-helm-retrieve-links-special (c)
  (setq zotxt-helm-prefix
        (zotxt-helm-get-special-cite-type))
  (zotxt-helm-retrieve-links (list c) t))

(defun zotxt-helm-retrieve-links-add-prefix (c)
  (setq zotxt-helm-prefix
        (concat (read-string "Prefix: ") " "))
  (zotxt-helm-retrieve-links (list c) t))

(defun zotxt-helm-get-special-cite-type ()
  (helm-comp-read
   "Special type: "
   (cl-loop for x in zotxt-extra-special-cites
            collect (cons (symbol-name (cdr x))
                          (concat (car x) ":"))
            into speciallist
            finally return
            (append speciallist '(("suppress-author" . "-"))))
   :allow-nest))

(defvar zotxt-helm--insert-fullbib nil)
(defun zotxt-helm-retrieve-fullbib (_c)
  (setq zotxt-helm--insert-fullbib t)
  (zotxt-extra-get-fullbibliography-deferred (helm-marked-candidates)))

;;TODO, abstract last part of insert function so that it is possible
;; to do persistent insertions? (deferred makes it difficult)
;; (defun zotxt-helm-persistent-retrieve-links (_c)
;;   (with-current-buffer (marker-buffer mk)
;;     (insert (mapconcat #'identity (helm-marked-candidates) ", ") ", "))
;;   (helm-unmark-all)
;;   (helm-set-pattern "")
;;   )

;; TODO, fontification of matches should initially be done more according to the
;; zotero-match, not the fuzzy match? How can this be achieved?

;;;###autoload
(defun zotxt-helm-insert-reference-link (arg &optional initial-input)
  "With prefix ARG only use references in current buffer"
  (interactive "P")
  (setq zotxt-helm-prefix nil
        zotxt-helm--insert-fullbib nil
        zotxt-helm-current-refs (zotxt-helm-get-current-refs))
  (let ((mk (point-marker))
        (sources (if arg '(zotxt-helm-current-refs-source zotxt-helm-history-source)
                   `(zotxt-helm-current-refs-source
                     ,(zotxt-helm-source)
                     zotxt-helm-history-source))))
    (deferred:$
      (deferred:next
        (lambda ()
          (helm :sources sources
                :input initial-input
                :input-idle-delay (if arg helm-input-idle-delay
                                    zotxt-helm-input-idle-delay))))
      (deferred:nextc it
        (lambda (items)
          (with-current-buffer (marker-buffer mk)
            (goto-char (marker-position mk))
            (if zotxt-helm--insert-fullbib
                (insert (plist-get items :text)) ;; should be bib-string from zotxt-helm-retrieve-fullbib
              (zotxt-helm-insert-reference-links-to-items items))))))))

(defun zotxt-helm-insert-reference-links-to-items (items)
  (cl-loop for item in items do
           (let ((refstring (if (stringp item)
                                item
                              (org-zotxt-make-item-link item)))
                 (history-delete-duplicates t))
             (add-to-history 'zotxt-helm-history
                             (car ; stupid
                              (zotxt-helm-link-strings-to-disp-real refstring)))
             (setq refstring
                   (zotxt-helm--add-prefix-to-link refstring))
             (insert refstring)
             (zotxt-extra-maybe-edit-description)
             (insert " "))))

;; (defun zotxt-helm--maybe-alter-link (linkstring)
;;   (cond
;;    (zotxt-helm-special-cite-type
;;     (zotxt-helm--add-special-to-link linkstring))
;;    (zotxt-helm-prefix
;;     ()))
;;   )

(defun zotxt-helm--add-prefix-to-link (linkstring &optional prefix)
  (replace-regexp-in-string
   "@"
   (concat (or prefix zotxt-helm-prefix) ;; possibly nothing is added
           "@")
   linkstring t t))


(define-key org-zotxt-mode-map (kbd "C-c \" s") #'zotxt-helm-add-special-to-link-at-point)

(defun zotxt-helm-add-special-to-link-at-point ()
  (interactive)
  (zotxt-extra-edit-link-at-point
   (setq desc
         (zotxt-helm--add-prefix-to-link
          desc
          (zotxt-helm-get-special-cite-type)))))


(defun zotxt-helm-insert-citekeys (arg)
  (interactive "P")
  (insert (mapconcat #'identity (zotxt-helm-get-citekeys arg) ",")))

(defun zotxt-helm-get-citekeys (arg &optional search)
  (interactive "P")
  (setq zotxt-helm-current-refs (zotxt-helm-get-current-refs))
  (let ((sources (if arg '(zotxt-helm-current-refs-source zotxt-helm-history-source)
                   `(zotxt-helm-current-refs-source
                     ,(zotxt-helm-source)
                     zotxt-helm-history-source))))
    (deferred:$
      (deferred:next
        (lambda ()
          (helm :sources sources
                :input search
                :input-idle-delay (if arg helm-input-idle-delay
                                    zotxt-helm-input-idle-delay))))
      (deferred:nextc it
        (lambda (items)
          (if (null items)
              (user-error "no items")
            (cl-loop for item in items
                     collect
                     (let ((refstring (if (stringp item)
                                          item
                                        (org-zotxt-make-item-link item)))
                           (history-delete-duplicates t))
                       (add-to-history 'zotxt-helm-history
                                       (car ; stupid
                                        (zotxt-helm-link-strings-to-disp-real refstring)))
                       (zotxt-helm--extract-citekey refstring))))))
      (deferred:sync! it))))

(defun zotxt-helm--extract-citekey (link)
  (when (string-match org-pdcite-citekey-re link)
    (match-string 1 link)))


(provide 'zotxt-helm)

;;; zotxt-helm.el ends here
