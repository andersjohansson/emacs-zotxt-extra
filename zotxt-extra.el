;;; zotxt-extra.el --- Extensions to org-zotxt  -*- lexical-binding: t -*-
;; Copyright (C) 2017 Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Version: 0.1
;; Created: 2017-07-21
;; Modified: 2021-09-02
;; Package-Requires: ((emacs "25.1") (org "9.3") (s "1.10.0") (zotxt "0.0"))
;; Keywords: outlines, wp
;; URL: https://www.gitlab.com/andersjohansson/zotxt-emacs-extra

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; zotxt-extra defines several useful extras for org-zotxt. Autoexport
;; of .bib-files, export to Zotero rtf2odf-parseable odt-files,
;; generated citations and bibliographies for exported html and a
;; few utility functions.


;; TODO - Could generalize the merging functionality so that formats
;; with formatted citations fetches all the citations in a filter in
;; org-export-filter-final-output-functions and replaces markers
;; placed into the text (like the {{{key lp}}} markers used for odt-scannable-cite)
;; The easy thing there is that merging of consecutive cites are done
;; after inserting the scannable cite keys.
;; But for formatted keys we would like to merge any adjacent
;; {{{}}}-markers to retrieve a citationList (or optionally even
;; bibliography-list if :fullcite is called for). Then we can’t make a
;; nice alist from citekey to the correct (single) citation to insert.
;; We need to know which combinations of citekeys we need to generate
;; citationLists for. This means they need to be generated after we’ve
;; determined which citations are adjacent. A trickier problem. This
;; can’t be done in the link-export function, but must be done in a
;; full scan of the finished document. But perhaps the markers could
;; just be inserted in this case. The document scanned through. A list
;; of citekey-combinations be generated. All citekeys retrieved (but
;; again, this can’t be done in a single call, that is the speedup for
;; odt-formatted, that it is easy to split them up afterwards, but we
;; can’t recieve one big list of citation-lists (without significant
;; new modifications to zotxt)). So we don’t save that much here. And
;; it’s a very complex problem.
;; Conclusion: The backends recieving formatted citations will have to
;; continue to do so in the dumb slow way for a while.

(require 'zotxt)
(require 'org-zotxt)
(require 'org-pdcite)
(require 's)
(require 'ox)
(require 'subr-x)
(require 'cl-lib)

;;; Variables
(defgroup zotxt-extra nil
  "Customizations for extra features"
  :group 'zotxt)

(defcustom zotxt-extra-nobiblatex nil
  "Just use cite instead of autocites when exporting to latex"
  :type 'boolean
  :safe 'booleanp)

(defvar zotxt-extra-special-cites
  '(("t" . :textcite)
    ("npt" . :nptextcite)
    ("title" . :citetitle)
    ("a" . :citeauthor)
    ("A" . :Citeauthor)
    ("fa" . :citefullauthor)
    ("y" . :citeyear)
    ("f" . :fullcite)
    ("no" . :nocite)
    ("bib" . :autobib)))

(defvar zotxt-extra-special-cites-regexp
  (concat (regexp-opt (mapcar #'car zotxt-extra-special-cites) t) ":"))

(defcustom zotxt-extra-autobib-export t
  "Whether to generate a bibtex file from found zotxt-references
  in exported org buffer"
  :type 'boolean
  :safe 'booleanp)

(defcustom zotxt-extra-generate-html-bib t
  "Whether to generate a bibliography in html exports"
  :type 'boolean
  :safe 'booleanp)

(defcustom zotxt-extra-generate-ascii-bib t
  "Whether to generate a bibliography in ascii exports"
  :type 'boolean
  :safe 'booleanp)


(defcustom zotxt-extra-autobib-translator "f895aa0d-f28e-47fe-b247-2ea77c6ed583"
  "Translator id to use for fetching bib(la)tex data from Zotero"
  :safe 'stringp
  :type '(choice
          (string :tag "Custom")
          (const :tag "BibTeX" "9cb70025-a888-4a29-a210-93ec52da40d4")
          (const :tag "BibLaTeX" "b6e39b57-8942-4d11-8259-342c46ce395f")
          (const :tag "Better-bibtex" "ca65189f-8815-4afe-8c8b-8c7c15f0edca")
          (const :tag "Better-biblatex" "f895aa0d-f28e-47fe-b247-2ea77c6ed583")))

(defcustom zotxt-extra-autobib-dispoptions nil
  "Translator displayOptions to use for fetching bib(la)tex data from Zotero
Requires custom fork of zotxt."
  :safe 'stringp
  :type '(choice
          (string :tag "Custom")
          (const :tag "journalAbbreviations" "useJournalAbbreviation:true")))

(defcustom zotxt-extra-autobib-filename-suffix ".auto.bib"
  "Filename suffix for automatically generated bib file
Should in all reasonable cases end with .bib"
  :type 'string
  :safe 'stringp)

;; FIXME, should perhaps be drawn from the org-export buffer language,
;; but those are language codes which then would need to be mapped to
;; locales. In addition, that information (info channel) is not
;; available in the link export functions. This way, the user can set
;; it as a file-local variable or/and with #+BIND-keyword
(defcustom zotxt-extra-csl-locale "en-US"
  "Locale used for generated CSL bibliographies."
  :type 'string
  :safe 'stringp)

(defcustom zotxt-extra-odt-cite-format-function #'zotxt-extra--odt-scannable-cite
  "Format function for odt-export.
Scannable cites, formatted-citation, or key in parentheses."
  :type '(choice
          (const :tag "Scannable cites" zotxt-extra--odt-scannable-cite)
          (const :tag "Formatted citation" zotxt-extra--odt-formatted-citation)
          (const :tag "Only citekey" zotxt-extra--odt-key))
  :safe 'functionp)

(defcustom zotxt-extra-odt-generate-bibliography t
  "Whether to generate a bibliography when not using scannable
citations for odt, i.e. when `zotxt-extra-odt-scannable-cite' is nil"
  :type 'boolean
  :safe 'booleanp)

(defvar zotxt-extra-autobib-collected-keys nil
  "Keys for references in org-file currently exported to latex,
which should be exported to bib(la)tex.")

;;; Custom links and autoexport
;;;; Override zotxt-link export setting
(defvar zotxt-extra-links-defined nil)
(defun zotxt-extra-define-links ()
  (org-link-set-parameters "zotero"
                           :export #'zotxt-extra-link-export))
(advice-add 'org-zotxt--define-links :after #'zotxt-extra-define-links)

;; Define early if mode is not activated, could be needed before export.
(zotxt-extra-define-links)

;;;; Link definition
;;;###autoload
(defun zotxt-extra-link-export (path desc format)
  (let ((lp (zotxt-extra-link-parse-desc
             (replace-regexp-in-string
              "\\\\_" "_"
              (substring-no-properties desc)))))
    (if (null lp)
        (progn
          (display-warning 'zotxt
                           (format "Failed parsing citation %s - %s"
                                   path desc))
          "(FAILED PARSING CITATION)")
      (cond ((eq format 'odt)
             (funcall zotxt-extra-odt-cite-format-function path lp))
            ((member format '(latex beamer))
             (zotxt-extra--latex-link path lp))
            ((eq format 'html)
             (zotxt-extra--html-link path lp))
            ((eq format 'md) ; TODO, could add support for in-text-citations
             (format "[%s]" desc)) ; should work reasonably with
                                        ; pandoc-citeproc
            ((eq format 'ascii)
             (zotxt-extra--ascii-link path lp))
            (t (format "[%s]" desc))))))


;;;; Odt scannable cite
(defvar zotxt-extra-odt-scannable-cite-keys nil
  "Holds the encountered keys in an odt scannable cites export")

(defun zotxt-extra--odt-scannable-cite (path lp)
  "Embed zotero key and parsed citation in odt output.
This is later replaced in
‘zotxt-extra--odt-scannable-cite-insert’ which is run via
‘org-export-filter-final-output-functions’."
  (or (zotxt-extra--no-specials-check
       path lp #'zotxt-extra--odt-comment
       :textcite :citeauthor :Citeauthor)
      (let ((key (org-zotxt-extract-link-id-from-path path)))
        (cl-pushnew key zotxt-extra-odt-scannable-cite-keys :test #'equal)
        (format "{{{%s %s}}}" key (prin1-to-string lp)))))

(defun zotxt-extra--odt-scannable-cite-insert (string backend _info)
  "Replace embedded zotero keys and citation info in odt output"
  (when (and (eq backend 'odt)
             (eq zotxt-extra-odt-cite-format-function
                 #'zotxt-extra--odt-scannable-cite)
             zotxt-extra-odt-scannable-cite-keys)
    (let* ((cite-codes
            (replace-regexp-in-string
             "&" "&amp;"
             (deferred:$
               (zotxt-get-item-deferred
                (list :key (mapconcat #'identity zotxt-extra-odt-scannable-cite-keys ","))
                :248bebf1-46ab-4067-9f93-ec3d2960d0cd)
               (deferred:nextc it
                 (lambda (item)
                   (plist-get item :248bebf1-46ab-4067-9f93-ec3d2960d0cd)))
               (deferred:sync! it))))
           (cite-codes-split
            (cl-loop for s in (split-string cite-codes "}{")
                     collect
                     (let* ((spl (split-string s "{\\||\\|}" t " +"))
                            ;; this assumes the option
                            ;; ODFScan.useZoteroSelect is not set
                            (code (nth 2 (split-string (cadr spl) ":"))))
                       (cons code spl))))
           (cite-codes-alist
            (cl-loop for k in zotxt-extra-odt-scannable-cite-keys
                     collect
                     (cons
                      k
                      (cdr
                       ;; here, we assume that we won’t have any equal
                       ;; keys in different zotero libraries, I guess
                       ;; it could happen but it is probably unlikely
                       (assoc k cite-codes-split
                              (lambda (el key)
                                (equal
                                 (string-trim-left key "[0-9]+_")
                                 el))))))))
      (with-temp-buffer
        (insert string)
        (goto-char (point-min))
        (while (search-forward-regexp
                "{{{\\([^[:space:]]+\\)[[:space:]]+\\([^}]+\\)}}}" nil t)
          (replace-match (zotxt-extra--odt-scannable-cite-format
                          (cdr (assoc (match-string 1) cite-codes-alist))
                          (read (match-string 2)))
                         t t))
        (buffer-string)))))

(add-to-list 'org-export-filter-final-output-functions
             #'zotxt-extra-merge-consecutive-cites)

;; has to be added before merge (not properly ensured by this really...)
(add-to-list 'org-export-filter-final-output-functions
             #'zotxt-extra--odt-scannable-cite-insert)

(defun zotxt-extra--odt-scannable-cite-format (sc-cite lp)
  (save-match-data
    (let* (;; (rtos (split-string
           ;;        sc-cite
           ;;        "{\\||\\|}" nil " +"))
           (refname (replace-regexp-in-string ", &" " &amp;" (car sc-cite)))
           (code (cadr sc-cite))
           (authors (s-with refname
                      (replace-regexp-in-string "\\(.+\\), [0-9]\\{4,4\\}" "\\1 ")
                      (s-replace ", et al." " et al."))))
      (decode-coding-string ; TODO, this is probably needed more
                                        ; generally, Check this.
       (format "%s{%s | %s | %s %s | %s |%s}"
               (cond
                ((plist-get lp :textcite)
                 (zotxt-extra--odt-comment "Check: Authorname correct?" authors))
                ((or (plist-get lp :citeauthor) (plist-get lp :Citeauthor))
                 (zotxt-extra--odt-comment "Check:Only author, remove ref. " authors))
                (t ""))
               (or (plist-get lp :prefix) " ")
               (concat (when (or (plist-get lp :textcite )
                                 (plist-get lp :suppress-author)) "-")
                       refname)
               (or (plist-get lp :locator-word) " ")
               (or (plist-get lp :locator) " ")
               (or (plist-get lp :suffix) " ")
               code)
       'utf-8)
      )))

;;;; Odt formatted citation

;; stupid and slow (because of sync)
(defun zotxt-extra--odt-formatted-citation (path lp)
  (when zotxt-extra-odt-generate-bibliography
    (zotxt-extra--add-autobib path))
  (or (when (plist-get lp :autobib) "")
      (zotxt-extra--no-specials-check path lp #'zotxt-extra--odt-comment)
      (deferred:$
        (zotxt-extra-get-item-citation-deferred
         `(:key ,(org-zotxt-extract-link-id-from-path path)))
        (deferred:nextc it
          (lambda (item)
            (concat
             "<text:span text:style-name=\"zotcite\">"
             (org-odt--encode-plain-text
              (replace-regexp-in-string "[\r\n]" ""
                                        (plist-get item :text))
              t)
             "</text:span>")))
        (deferred:sync! it))))

;;;; ODT: simple output of the  citekey.
(defun zotxt-extra--odt-key (_path lp)
  (concat "<text:span text:style-name=\"zotcite\">("
          (plist-get lp :citekey)
          ")</text:span>"))

;;;; LaTeX

(defun zotxt-extra--latex-link (path lp)
  (when zotxt-extra-autobib-export
    (zotxt-extra--add-autobib path))
  (cond
   ((plist-get lp :autobib) "") ;; no output, only possibly
   ;; include in autobib
   ((plist-get lp :nocite)
    (format "\\nocite{%s}" (plist-get lp :citekey)))
   (t (format
       (cond
        (zotxt-extra-nobiblatex "\\cite[%s][%s]{%s}")
        ((plist-get lp :textcite) "\\textcite[%s][%s]{%s}")
        ((plist-get lp :nptextcite) "\\nptextcite[%s][%s]{%s}") ;; biblatex-apa
        ((plist-get lp :citeauthor) "\\citeauthor[%s][%s]{%s}")
        ((plist-get lp :Citeauthor) "\\Citeauthor[%s][%s]{%s}")
        ((plist-get lp :citefullauthor) "\\citefullauthor[%s][%s]{%s}")
        ((plist-get lp :citetitle) "\\citetitle[%s][%s]{%s}")
        ((plist-get lp :citeyear) "\\citeyear[%s][%s]{%s}")
        ((plist-get lp :fullcite) "\\fullcite[%s][%s]{%s}")
        ((plist-get lp :suppress-author) "\\autocite*[%s][%s]{%s}")
        (t "\\autocite[%s][%s]{%s}"))
       (or (plist-get lp :prefix) "")
       (let ((lw (plist-get lp :locator-word))
             (lo (plist-get lp :locator))
             (su (plist-get lp :suffix)))
         (if lo
             (concat
              (when lw
                (when (or zotxt-extra-nobiblatex
                          (not (member lw '("p." "pp." "s."))))
                  ;; No need
                  ;; to print these explicitly, maybe
                  ;; more could be added
                  (concat lw "~"))) ; add no-breaking-space
              lo
              (when su (concat "; " su))) ; locator+suffix. Possible?
           (or su ""))) ; only suffix
       (plist-get lp :citekey)))))

;;;; HTML

(defun zotxt-extra--html-link (path lp)
  (when zotxt-extra-generate-html-bib
    (zotxt-extra--add-autobib path))
  (or (when (plist-get lp :autobib) "")
      (zotxt-extra--no-specials-check
       path lp (lambda (comment text) (format "(<emph>%s</emph>, %s)" text comment))
       :fullcite)
      (let ((key (org-zotxt-extract-link-id-from-path path)))
        (deferred:$
          (if (plist-get lp :fullcite)
              (zotxt-extra-get-fullbibliography-deferred (list key))
            (zotxt-extra-get-item-citation-deferred `(:key ,key)))
          (deferred:nextc it
            (lambda (item)
              (format "<span class=\"zotcite\">%s</span>"
                      (plist-get item :html))))
          (deferred:sync! it)))))

;;;; Text (ascii)

(defun zotxt-extra--ascii-link (path lp)
  (when zotxt-extra-generate-ascii-bib
    (zotxt-extra--add-autobib path))
  (or (when (plist-get lp :autobib) "")
      (zotxt-extra--no-specials-check
       path lp (lambda (comment text) (format "(*%s*, %s)" text comment))
       :fullcite)
      (let ((key (org-zotxt-extract-link-id-from-path path)))
        (deferred:$
          (if (plist-get lp :fullcite)
              (zotxt-extra-get-fullbibliography-deferred (list key))
            (zotxt-extra-get-item-citation-deferred `(:key ,key)))
          ;; (deferred:call func
          ;;   `(:key ,(org-zotxt-extract-link-id-from-path path)))
          (deferred:nextc it
            (lambda (item)
              (plist-get item :text)))
          (deferred:sync! it)))))

;;;; General utility functions for export
(defun zotxt-extra--add-autobib (path)
  (cl-pushnew
   (org-zotxt-extract-link-id-from-path path)
   zotxt-extra-autobib-collected-keys
   :test #'equal))

(defun zotxt-extra--no-specials-check (path lp commentfn &rest supported)
  (let* ((special-cites (mapcar #'cdr zotxt-extra-special-cites))
         (no-support
          (pcase supported
            ('(all) nil)
            ((or 'nil '(none))
             special-cites)
            (_
             (cl-set-difference special-cites supported)))))
    (cl-loop for x in no-support
             when (plist-get lp x)
             return
             (progn
               (display-warning
                'zotxt
                (format "No support for %s in current export. Item: %s,%s"
                        x path (plist-get lp :citekey)))
               (funcall commentfn
                        (format "NO %s" (upcase (symbol-name x)))
                        (plist-get lp :citekey))))))


(defun zotxt-extra--odt-comment (comment text)
  (let ((an-name (concat "__Annot_" (number-to-string (random)))))
    (format "<office:annotation office:name=\"%s\"><dc:creator>%s</dc:creator><dc:date>%s</dc:date><text:list><text:list-item><text:p>%s</text:p></text:list-item></text:list></office:annotation>%s<office:annotation-end office:name=\"%s\"/>"
            an-name
            "zotxt-extra"
            (let ((ct (current-time)))
              (concat (format-time-string "%FT%T." ct) (number-to-string (nth 2 ct))))
            comment text an-name)))

;;; Getters for formatted citations and bibliographies
;; NOTE! These requires a a forked version of zotxt
;; https://gitlab.com/andersjohansson/zotxt/tree/csl

(defun zotxt-extra-get-item-citation-deferred (item &optional style)
  "Retrieve the generated citation for ITEM (a plist).
Use STYLE to specify a custom bibliography style.
Returns a plist with :text, :html, and :rtf entries.

For use only in a `deferred:$' chain."
  (let ((d (deferred:new))
        (style (or style zotxt-default-bibliography-style))
        (item item))
    (request
     (format "%s/items" zotxt-url-base)
     :params `(("key" . ,(plist-get item :key))
               ("format" . "citationList")
               ("style" . ,style)
               ("locale" . ,zotxt-extra-csl-locale))
     :parser #'zotxt--json-read
     :success (cl-function
               (lambda (&key data &allow-other-keys)
                 (plist-put item :text (cdr (assq 'text data)))
                 (plist-put item :html (cdr (assq 'html data)))
                 (plist-put item :rtf (cdr (assq 'rtf data)))
                 (deferred:callback-post d item))))
    d))


(defun zotxt-extra-get-fullbibliography-deferred (keys &optional style)
  "Retrieve the generated bibliography for keys in KEYS
Use STYLE to specify a custom bibliography style.
Returns a plist with :text, :html, and :rtf entries.
For use only in a `deferred:$' chain."
  (let ((d (deferred:new))
        (style (or style zotxt-default-bibliography-style))
        (keys (mapconcat #'identity keys ","))
        (bib '(:cslformatted t)))
    (request
     (format "%s/items" zotxt-url-base)
     :params `(("key" . ,keys)
               ("format" . "fullBibliography")
               ("style" . ,style)
               ("locale" . ,zotxt-extra-csl-locale))
     :parser #'zotxt--json-read
     :success (cl-function
               (lambda (&key data &allow-other-keys)
                 (plist-put bib :text (cdr (assq 'text data)))
                 (plist-put bib :html (cdr (assq 'html data)))
                 (plist-put bib :rtf (cdr (assq 'rtf data)))
                 (deferred:callback-post d bib))))
    d))

(defun zotxt-extra-get-item-bibliographies-deferred (keys &optional style)
  "Retrieve the generated bibliography for keys in KEYS.
Use STYLE to specify a custom bibliography style.
Adds a plist entry with the name of the style as a self-quoting symbol, e.g.
Returns a plist with :text, :html, and :rtf entries.

For use only in a `deferred:$' chain."
  (let ((d (deferred:new))
        (style (or style zotxt-default-bibliography-style))
        (keys (mapconcat #'identity keys ","))
        bib)
    (request
     (format "%s/items" zotxt-url-base)
     :params `(("key" . ,keys)
               ("format" . "bibliography")
               ("style" . ,style)
               ("locale" . ,zotxt-extra-csl-locale))
     :parser #'zotxt--json-read
     :success (cl-function
               (lambda (&key data &allow-other-keys)
                 (cl-loop for i across data do
                          ;; (plist-put b :text (cdr (assq 'text i)))
                          ;; (plist-put b :html (cdr (assq 'html i)))
                          ;; (plist-put b :rtf (cdr (assq 'rtf i)))
                          (push (cons (cdr (assq 'key i))
                                      (list
                                       :text (cdr (assq 'text i))
                                       :html (cdr (assq 'html i))
                                       :rtf (cdr (assq 'rtf i))))

                                bib))
                 (deferred:callback-post d bib))))
    d))

;;; Parse function for links

(defun zotxt-extra-link-parse-desc (desc)
  "Parses zotxt link description.
Looks for special citation markers from
 `zotxt-extra-special-cites' and then uses pdcite parser
 `org-pdcite-full-cite-parser'."
  (with-temp-buffer
    (insert desc)
    (goto-char (point-min))
    (let (pl)
      (when (search-forward-regexp zotxt-extra-special-cites-regexp nil t)
        ;;anything before a special cite token gets excluded
        (setq pl (plist-put
                  pl
                  (cdr (assoc (match-string 1) zotxt-extra-special-cites))
                  t)))
      (append pl (org-pdcite-full-cite-parser)))))

;;; Merge consecutive cites
(defun zotxt-extra-merge-consecutive-cites (string backend _info)
  (cond ((member backend '(latex beamer))
         (if zotxt-extra-nobiblatex
             (zotxt-extra-replace-consecutive-latex string "cite" "cite" "[ ,]*")
           (thread-first
               string
             (zotxt-extra-replace-consecutive-latex
              "autocite\\*" "autocite*" "[ ,]*")
             (zotxt-extra-replace-consecutive-latex
              "autocite" "autocite" "[ ,]*" "autocites"))))
        ((eq backend 'odt)
         (cl-case zotxt-extra-odt-cite-format-function
           (zotxt-extra--odt-scannable-cite
            (replace-regexp-in-string "}[ 	]+{" "}{" string))
           (_
            (zotxt-extra--replace-consecutive
             string
             "<text:span text:style-name=\"zotcite\">(\\([^<\n]+\\))</text:span>"
             "[[:space:]]*"
             "<text:span text:style-name=\"zotcite\">(%s)</text:span>"
             "; "))))
        ((org-export-derived-backend-p backend 'html)
         ;; This just assumes some author-year (parenthesis) style
         (zotxt-extra--replace-consecutive
          string
          "<span class=\"zotcite\">(\\([^<\n]+\\))</span>"
          "[[:space:]]*"
          "<span class=\"zotcite\">(%s)</span>"
          "; "))
        ((org-export-derived-backend-p backend 'ascii)
         ;; This just assumes some author-year (parenthesis) style
         (zotxt-extra--replace-consecutive
          string
          "(\\([^)\n]+\\))"
          "[[:space:]]*"
          "(%s)"
          "; "))
        (t string)))

(defun zotxt-extra--replace-consecutive (string re allowedbetween wrap delim)
  (let ((nextre (concat allowedbetween re)))
    (with-temp-buffer
      (insert string)
      (goto-char (point-min))
      (while (search-forward-regexp re nil t)
        (let ((citelist (list (match-string 1)))
              (point-at-first (match-beginning 0)))
          (while (looking-at nextre)
            (push (match-string 1) citelist)
            (goto-char (match-end 0)))
          (delete-region point-at-first (match-end 0))
          (goto-char point-at-first)
          (insert (format wrap
                          (mapconcat #'identity (nreverse citelist) delim)))))
      (buffer-string))))

(defun zotxt-extra-replace-consecutive-latex (string cmd newcmd allowedbetween &optional newcmdmulti)
  (let* ((re (format "\\\\%s\\(?:\\[\\(?2:[^]]*\\)\\]\\)?\\(?:\\[\\(?3:[^]]*\\)\\]\\)?{\\(?1:[^,}]+\\)}" cmd))
         (nextre (concat allowedbetween re)))
    (with-temp-buffer
      (insert string)
      (goto-char (point-min))
      (while (search-forward-regexp re nil t)
        (cl-block foundseq
          (let ((citelist (list (list (match-string 1) (match-string 2) (match-string 3))))
                (startpos (match-beginning 0))
                replacement)
            (while (looking-at nextre)
              (push (list (match-string 1) (match-string 2) (match-string 3)) citelist)
              (goto-char (match-end 0)))
            (setq citelist (nreverse citelist))
            (when (< 1 (length citelist))
              (if (and (cl-every (lambda (cite) (and (s-blank-p (nth 1 cite))
                                                (s-blank-p (nth 2 cite))))
                                 (cdr citelist))
                       (s-blank-p (nth 2 (car citelist))))
                  (setq replacement (format "\\%s%s{%s}" newcmd
                                            (if (s-blank-p (nth 1 (car citelist)))
                                                ""
                                              (concat "[" (nth 1 (car citelist)) "][]"))
                                            (mapconcat #'car citelist ",")))
                (if newcmdmulti
                    (setq replacement
                          (format "\\%s%s" newcmdmulti
                                  (mapconcat
                                   (lambda (c) (format "[%s][%s]{%s}"
                                                  (nth 1 c) (nth 2 c) (car c)))
                                   citelist "")))
                  ;; otherwise it’s unsafe to do anything
                  (cl-return-from foundseq)))
              (delete-region startpos (match-end 0))
              (goto-char startpos)
              (insert replacement)))))
      (buffer-string))))


;;; Exporting of bib-files or generation of html-bibliography

(add-to-list 'org-export-filter-final-output-functions
             #'zotxt-extra-make-bibliography-or-bibfile)
(add-hook 'org-export-before-processing-hook #'zotxt-extra-reset)

(defun zotxt-extra-reset (_backend)
  (setq zotxt-extra-autobib-collected-keys nil)
  (setq zotxt-extra-odt-scannable-cite-keys nil))

(defun zotxt-extra-make-bibliography-or-bibfile (string backend info)
  (cond
   ((and zotxt-extra-autobib-export
         (member backend '(latex beamer))
         zotxt-extra-autobib-collected-keys)
    (zotxt-extra--make-bibfile string info))
   ((and zotxt-extra-generate-html-bib
         (org-export-derived-backend-p backend 'html)
         zotxt-extra-autobib-collected-keys)
    (zotxt-extra--generate-html-bib string info))
   ((and zotxt-extra-generate-ascii-bib
         (org-export-derived-backend-p backend 'ascii)
         zotxt-extra-autobib-collected-keys)
    (zotxt-extra--generate-ascii-bib string info))
   ((and zotxt-extra-odt-generate-bibliography
         (org-export-derived-backend-p backend 'odt)
         zotxt-extra-autobib-collected-keys)
    (zotxt-extra--generate-odt-bib string info))
   (t string)))

(defun zotxt-extra--make-bibfile (string info)
  "Exports a bib-file with bib(la)tex-data collected for the
zotxt-references in the exported org buffer"
  (let* ((keys (mapconcat #'identity zotxt-extra-autobib-collected-keys ","))
         (trans (intern (concat ":" zotxt-extra-autobib-translator)))
         (bibfile (concat
                   (file-name-base (or
                                    (plist-get info :output-file)
                                    (plist-get info :input-file)
                                    (replace-regexp-in-string
                                     "*" ""
                                     (plist-get info :input-buffer))))
                   zotxt-extra-autobib-filename-suffix))
         (bibstring
          (deferred:$
            (zotxt-get-item-deferred `(:key ,keys) trans zotxt-extra-autobib-dispoptions)
            (deferred:nextc it
              (lambda (item)
                (plist-get item trans)))
            (deferred:sync! it)))) ; stupid as always
    (when bibstring
      (with-current-buffer (generate-new-buffer bibfile)
        (set-buffer-file-coding-system 'no-conversion)
                                        ; some strange encoding errors
                                        ; but when reverting file it is
                                        ; correct utf8
        (insert bibstring)
        (write-file bibfile)
        (kill-buffer))
      (if zotxt-extra-nobiblatex
          (replace-regexp-in-string
           "\\\\bibliography{[^}]+}"
           (format "\\bibliography{%s}" (file-name-base bibfile)) string t t)
        (replace-regexp-in-string
         "\\\\begin{document}"
         (format "\\addbibresource{%s}\n\\begin{document}" bibfile)
         string t t)))))

(defun zotxt-extra--generate-ascii-bib (string _info)
  (let ((bibstring (zotxt-extra--get-full-autobib :text))
        (case-fold-search nil))
    (when bibstring
      (replace-regexp-in-string "BIBLIOGRAPHY" bibstring string t t))))

(defun zotxt-extra--generate-html-bib (string info)
  (when-let ((bibstring (zotxt-extra--get-full-autobib :html)))
    (setq bibstring (zotxt-extra--split-html-reveal-bib bibstring info))
    (replace-regexp-in-string
     "<div id=\"zotxtbibliography\"></div>"
     (concat "<div id=\"zotxtbibliography\">" bibstring "</div>")
     string t t)))

(defun zotxt-extra--split-html-reveal-bib (string info)
  (if (org-export-derived-backend-p (plist-get info :back-end) 're-reveal 'reveal)
      (with-temp-buffer
        (insert string)
        (goto-char (point-min))
        (cl-loop with i = 0
                 with positions = nil
                 while (search-forward "<div class=\"csl-entry\">" nil t) do
                 (cl-incf i)
                 (when (eq 0 (mod i 6))
                   (push (match-beginning 0) positions))
                 finally do
                 (cl-loop for pos in positions do
                          (goto-char pos)
                          (insert "</div></div></section><section><h2>References cont.</h2><div id=\"zotxtbibliography\"><div class=\"csl-bib-body\">")))
        (buffer-string))
    string))

(defun zotxt-extra--generate-odt-bib (string _info)
  (when-let* ((bibstring (zotxt-extra--get-full-autobib :text))
              (bibsplit (split-string bibstring "\n" t)))
    (with-temp-buffer
      (insert string)
      (goto-char (point-max))
      (when (search-backward "</office:text>" nil t)
        (insert
         (concat
          (format
           "\n<text:h text:style-name=\"%s\" text:outline-level=\"%s\" text:is-list-header=\"%s\">%s</text:h>"
           "Heading_20_1_unnumbered" 1 "false" "References")
          (cl-loop for x in bibsplit
                   concat
                   (concat
                    "<text:p text:style-name=\"Bibliography_20_1\">"
                    (org-odt--encode-plain-text x t)
                    "</text:p>"))))
        (buffer-string)))))

(defun zotxt-extra--get-full-autobib (type)
  (deferred:$
    (zotxt-extra-get-fullbibliography-deferred zotxt-extra-autobib-collected-keys)
    (deferred:nextc it
      (lambda (bib)
        (plist-get bib type)))
    (deferred:sync! it)))

;;; Update, better suited for me, since it keeps page numbers etc.
(defun zotxt-extra-update-reference-link-at-point ()
  "Update the zotero:// link at point. Only use for citekey-style!"
  (interactive)
  (if (eq org-zotxt-link-description-style :citekey)
      (let ((mk (point-marker))
            (item-id (org-zotxt-extract-link-id-at-point)))
        (when item-id
          (deferred:$
            (deferred:next (lambda () `(:key ,item-id)))
            (deferred:nextc it
              (lambda (item)
                (zotxt-get-item-deferred item org-zotxt-link-description-style)))
            (deferred:nextc it
              (lambda (item)
                (save-excursion
                  (with-current-buffer (marker-buffer mk)
                    (goto-char (marker-position mk))
                    (let ((ct (org-element-context)))
                      (goto-char (org-element-property :begin ct))
                      (when (search-forward-regexp org-pdcite-citekey-re
                                                   (org-element-property :end ct) t)
                        (replace-match
                         (concat "@" (plist-get item :citekey)))))))))
            (if zotxt--debug-sync (deferred:sync! it)))))
    (user-error "Only works if `org-zotxt-link-description-style' = :citkey")))

;;;###autoload
(defun zotxt-extra-update-all-reference-links ()
  "Update all zotero:// links in a document or region.
If using citekey style"
  (interactive)
  (if (eq org-zotxt-link-description-style :citekey)
      (save-excursion
        (save-restriction
          (when (use-region-p)
            (narrow-to-region (region-beginning) (region-end)))
          (goto-char (point-max))
          (while (re-search-backward org-link-bracket-re nil t)
            (let* ((parse (org-element-link-parser))
                   (path (org-element-property :path parse)))
              (when (org-zotxt-extract-link-id-from-path path)
                (message "[zotxt] updating path: %s, at point %d" path (point))
                (zotxt-extra-update-reference-link-at-point))))))
    (user-error "Only works if `org-zotxt-link-description-style' = :citekey")))

;;; Function for updating ids
(defun zotxt-extra-update-library-ids (oldid newid)
  (interactive "nOld id: \nnNew id: ")
  "Change library ids for zotero:// links in a document.
That is changing “[OLDID]_J7WMYHSL” to “[NEWID]_J7WMYHSL” in the links."
  (save-match-data
    (save-excursion
      (goto-char (point-max))
      (while (re-search-backward org-link-bracket-re nil t)
        (goto-char (match-beginning 1))
        (when
            (and
             (search-forward "zotero:" (match-end 1) t)
             (search-forward-regexp
              (format "%s_\\([A-Z0-9]\\{8,8\\}\\)" oldid)
              (match-end 1) t))
          (replace-match (format "%s_\\1" newid)))))))

;;; Function for showing complete citation of reference at point

(defun zotxt-extra-display-citation ()
  "Display complete citation of org-zotxt link at point"
  (interactive)
  (when-let ((key (org-zotxt-extract-link-id-at-point)))
    (deferred:$
      (zotxt-get-item-bibliography-deferred (list :key key))
      (deferred:nextc it
        (lambda (item)
          (message
           (plist-get item :citation)))))))

(define-key org-zotxt-mode-map (kbd "C-c \" c") #'zotxt-extra-display-citation)

;;;; Showing complete citation as help-echo (tooltip)
;; TODO, document

(defun zotxt-extra-complete-citation-help-echo-activate ()
  "Activate help-echo for zotero links showing complete citations."
  (interactive)
  (org-link-set-parameters "zotero" :help-echo #'zotxt-extra-help-echo))

(defun zotxt-extra-complete-citation-help-echo-deactivate ()
  "Deactivate help-echo for zotero links."
  (interactive)
  (org-link-set-parameters "zotero" :help-echo nil))

(defun zotxt-extra-help-echo (_win buf pos)
  "Help-echo function for zotero links in BUF at POS."
  (with-current-buffer buf
    (save-excursion
      (goto-char pos)
      (zotxt-extra--help-echo-citation ))))

(defvar zotxt-extra-full-citations-cache
  (make-hash-table :test 'equal)
  "Cache of full citations for help-echo.")

(defun zotxt-extra--help-echo-citation ()
  "Get full citation for link at point."
  (when-let ((key (org-zotxt-extract-link-id-at-point)))
    (pcase (gethash key zotxt-extra-full-citations-cache)
      ((and (pred stringp) c) c)
      ('waiting nil)
      (_ (progn
           (puthash key 'waiting zotxt-extra-full-citations-cache)
           (deferred:$
             (zotxt-get-item-bibliography-deferred (list :key key))
             (deferred:nextc it
               (lambda (item)
                 ;; we can’t know if this is from tooltip
                 ;; (mouse) or help-at-pt (keyboard), so in this case
                 ;; we always get a message.
                 (message
                  (puthash key (plist-get item :citation)
                           zotxt-extra-full-citations-cache)))))
           nil)))))

;;; Function for adding page reference if we inserted a reference
;;; inside or after a quote
;;;###autoload
(defun zotxt-extra-maybe-edit-description (&rest _args)
  (when (or (org-element-lineage (org-element-context) '(quote-block) t)
            (looking-back
             (concat "[\"”’][[:space:]]*"
                     org-link-bracket-re)
             (point-at-bol)))
    (zotxt-extra-add-link-page-reference)))

(defmacro zotxt-extra-edit-link-at-point (&rest body)
  "Edit LINK and DESC variables to edit link"
  `(when (and (org-in-regexp org-link-bracket-re 1) (match-end 2))
     (when-let ((link (match-string-no-properties 1))
                (desc (match-string-no-properties 2))
                (mb (match-beginning 0))
                (me (match-end 0)))
       ,@body
       (delete-region mb me)
       (insert (org-link-make-string link desc))
       (sit-for 0))))

(defun zotxt-extra-add-link-page-reference ()
  (interactive)
  (zotxt-extra-edit-link-at-point
   (when-let ((pn (read-string
                   (format "Page reference for %s: " desc)
                   "p. ")))
     (unless (string-blank-p pn)
       (setq desc (concat desc ", " pn))))))


;; (defun zotxt-extra-add-link-page-reference ()
;;   (when (and (org-in-regexp org-bracket-link-regexp 1) (match-end 3))
;;     (when-let ((link (match-string-no-properties 1))
;;                (desc (match-string-no-properties 3))
;;                (mb (match-beginning 0))
;;                (me (match-end 0))
;;                (pn (read-string (format "Page reference for %s: "
;;                                         (match-string-no-properties 3))
;;                                 "p. ")))
;;       (unless (string-blank-p pn)
;;         (delete-region mb me)
;;         (insert (org-make-link-string
;;                  link
;;                  (concat desc ", " pn)))
;;         (sit-for 0)))))

(provide 'zotxt-extra)

;;; zotxt-extra.el ends here
